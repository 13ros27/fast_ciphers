use fast_ciphers::analyse::*;
use fast_ciphers::clean_upper;
use fast_ciphers::refinement::Refined;
use typenum::{U65, U91};

fn get_text() -> Box<[Refined<u8, U65, U91>]> {
    clean_upper(
        &std::fs::read_to_string("texts/alice_in_wonderland.txt")
            .expect("Failed to read Alice in Wonderland"),
    )
}

#[test]
fn test_monogram_score() {
    assert_eq!(monogram_score(&get_text()), 306515370);
}

#[test]
fn test_aa() {
    use fast_ciphers::{Cipher, Crack};
    let mut text = clean_upper("HELLO THERE HOW ARE YOUHJSDHAJKDHASKJDASHDJKASDHKASJDHASJKADKSKDHASDASASAKJHKASKAHASDKDASJAHKASKDJAS");
    // let mut text = clean_upper("HELLO THERE HOW ARE YOU");
    fast_ciphers::Caesar.encrypt::<64>(&mut text, Refined::constant::<14>());
    fast_ciphers::Caesar.find_key::<64>(&mut text);
}
