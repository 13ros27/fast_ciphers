use criterion::{black_box, criterion_group, criterion_main, Criterion};
use fast_ciphers::refinement::{ConstRange, Refined};
use fast_ciphers::*;
use typenum::{U0, U26, U65, U91};

fn get_text() -> Box<[Refined<u8, U65, U91>]> {
    clean_upper(
        &std::fs::read_to_string("texts/alice_in_wonderland.txt")
            .expect("Failed to read Alice in Wonderland"),
    )
}

mod caesar {
    use super::*;

    fn decrypt(c: &mut Criterion) {
        c.bench_function(&format!("Caesar.decrypt, len {}", get_text().len()), |b| {
            let mut text = &mut get_text();
            Caesar.encrypt::<64>(&mut text, Refined::constant::<14>());
            b.iter(|| Caesar.decrypt::<64>(black_box(&mut text), Refined::constant::<14>()))
        });
    }

    fn encrypt(c: &mut Criterion) {
        let mut text = get_text();
        c.bench_function(&format!("Caesar.encrypt, len {}", text.len()), |b| {
            b.iter(|| Caesar.encrypt::<64>(black_box(&mut text), Refined::constant::<14>()))
        });
    }

    fn crack(c: &mut Criterion) {
        c.bench_function(&format!("Caesar.find_key, len {}", get_text().len()), |b| {
            let mut text = &mut get_text();
            Caesar.encrypt::<64>(&mut text, Refined::constant::<14>());
            b.iter(|| Caesar.find_key::<64>(black_box(&mut text)))
        });

        c.bench_function(&format!("Caesar.crack, len {}", get_text().len()), |b| {
            let mut text = &mut get_text();
            Caesar.encrypt::<64>(&mut text, Refined::constant::<14>());
            b.iter(|| Caesar.crack::<64>(black_box(&mut text)))
        });
    }

    criterion_group!(group, decrypt, encrypt, crack);
}

mod affine {
    use super::*;

    fn decrypt(c: &mut Criterion) {
        c.bench_function(&format!("Affine.decrypt, len {}", get_text().len()), |b| {
            let mut text = &mut get_text();
            Affine.encrypt::<64>(
                &mut text,
                (Refined::constant::<19>(), Refined::constant::<14>()),
            );
            b.iter(|| {
                Affine.decrypt::<64>(
                    black_box(&mut text),
                    (Refined::constant::<19>(), Refined::constant::<14>()),
                )
            })
        });
    }

    fn encrypt(c: &mut Criterion) {
        let mut text = get_text();
        c.bench_function(&format!("Affine.encrypt, len {}", text.len()), |b| {
            b.iter(|| {
                Affine.encrypt::<64>(
                    black_box(&mut text),
                    (Refined::constant::<19>(), Refined::constant::<14>()),
                )
            })
        });
    }

    fn crack(c: &mut Criterion) {
        c.bench_function(&format!("Affine.crack, len {}", get_text().len()), |b| {
            let mut text = &mut get_text();
            Affine.encrypt::<64>(
                &mut text,
                (Refined::constant::<19>(), Refined::constant::<14>()),
            );
            b.iter(|| Affine.crack::<64>(black_box(&mut text)))
        });
    }

    criterion_group!(group, decrypt, encrypt, crack);
}

mod vigenere {
    use super::*;

    fn decrypt(c: &mut Criterion) {
        c.bench_function(
            &format!("Vigenere.decrypt, len {}", get_text().len()),
            |b| {
                let mut text = &mut get_text();
                Vigenere.encrypt::<64>(
                    &mut text,
                    &vec![
                        Refined::constant::<3>(),
                        Refined::constant::<7>(),
                        Refined::constant::<1>(),
                        Refined::constant::<9>(),
                        Refined::constant::<12>(),
                        Refined::constant::<19>(),
                        Refined::constant::<7>(),
                    ],
                );
                b.iter(|| {
                    Vigenere.decrypt::<64>(
                        black_box(&mut text),
                        &vec![
                            Refined::constant::<3>(),
                            Refined::constant::<7>(),
                            Refined::constant::<1>(),
                            Refined::constant::<9>(),
                            Refined::constant::<12>(),
                            Refined::constant::<19>(),
                            Refined::constant::<7>(),
                        ],
                    )
                })
            },
        );
    }

    fn encrypt(c: &mut Criterion) {
        let mut text = get_text();
        c.bench_function(&format!("Vigenere.encrypt, len {}", text.len()), |b| {
            b.iter(|| {
                Vigenere.encrypt::<64>(
                    black_box(&mut text),
                    &vec![
                        Refined::constant::<3>(),
                        Refined::constant::<7>(),
                        Refined::constant::<1>(),
                        Refined::constant::<9>(),
                        Refined::constant::<12>(),
                        Refined::constant::<19>(),
                        Refined::constant::<7>(),
                    ],
                )
            })
        });
    }

    criterion_group!(group, decrypt, encrypt);
}

mod analyse {
    use super::*;

    fn monogram_score_(c: &mut Criterion) {
        let text = get_text();
        c.bench_function(
            &format!("analyse::monogram_score, len {}", text.len()),
            |b| b.iter(|| monogram_score(black_box(&text))),
        );
    }

    fn index_of_coincidence(c: &mut Criterion) {
        let text = get_text();
        c.bench_function(&format!("analyse::ioc, len {}", text.len()), |b| {
            b.iter(|| ioc(black_box(&text)))
        });
    }

    fn periodic_ioc_(c: &mut Criterion) {
        let text = get_text();
        c.bench_function(&format!("analyse::periodic_ioc, len {}", text.len()), |b| {
            b.iter(|| periodic_ioc(black_box(&text), 7))
        });
    }

    criterion_group!(group, monogram_score_, index_of_coincidence, periodic_ioc_);
}

criterion_main!(
    caesar::group,
    affine::group,
    vigenere::group,
    analyse::group
);
