// Trait for special modular arithmetic methods
pub trait Modular {
    // Modular increment (the increment must be smaller than the modulus)
    fn mod_inc(self, increment: Self, modulus: Self) -> Self;
}

impl Modular for usize {
    #[inline]
    #[must_use]
    fn mod_inc(self, increment: usize, modulus: usize) -> usize {
        let mut total = self + increment;
        if total >= modulus {
            total -= modulus;
        }
        total
    }
}
