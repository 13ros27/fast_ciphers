#![recursion_limit = "256"] // Thanks u128::SIZE
#![feature(associated_type_defaults, portable_simd)]
#![forbid(
    clippy::undocumented_unsafe_blocks,
    clippy::multiple_unsafe_ops_per_block
)]
#![deny(clippy::pedantic)]
#![allow(
    dead_code,
    clippy::cast_lossless,
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::module_name_repetitions
)]

pub mod affine;
pub mod analyse;
pub mod caesar;
pub mod clean;
mod modular;
pub mod refinement;
pub mod traits;
pub mod utils;
pub mod vigenere;

pub use affine::Affine;
pub use analyse::{ioc, monogram_score, periodic_ioc};
pub use caesar::Caesar;
pub use clean::clean_upper;
pub use traits::{Cipher, Crack, CrackKl, KeySpace};
pub use vigenere::Vigenere;
