use std::fmt::{self, Debug, Formatter};
use std::iter::FusedIterator;
use std::marker::PhantomData;
use std::mem::{size_of, ManuallyDrop};
use std::ops::{Add, AddAssign, Index, IndexMut, Mul, Range, Rem, Sub};
use std::ptr;
use std::simd::{
    cmp::{SimdPartialEq, SimdPartialOrd},
    num::SimdUint,
    LaneCount, Mask, Simd, SimdCast, SimdElement, SupportedLaneCount,
};
use typenum::{
    Add1, Const, GrEq, IsGreaterOrEqual, IsLessOrEqual, LeEq, NonZero, Pow, Sub1, ToInt, ToUInt,
    Unsigned, B1, U0, U16, U2, U256, U4, U8,
};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Refined<T, S: Unsigned, E: Unsigned>(T, PhantomData<(S, E)>); // Range-exclusive (S..E)

impl<T, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    /// # Safety
    /// `num` must be in S..E
    pub const unsafe fn new_unchecked(num: T) -> Self {
        Self(num, PhantomData)
    }

    #[inline]
    #[allow(clippy::wrong_self_convention)]
    pub fn to_int(self) -> T {
        self.0
        // // repr(transparent) wrapper (but we can't use self.0 because of Drop in const)
        // unsafe { cursed_transmute(self) }
    }

    #[inline]
    pub fn widen<S2, E2>(self) -> Refined<T, S2, E2>
    where
        S2: Unsigned + IsLessOrEqual<S>,
        E2: Unsigned + IsGreaterOrEqual<E>,
        LeEq<S2, S>: NonZero,
        GrEq<E2, E>: NonZero, // TODO: Could use special traits and `diagnostic::on_unimplemented` to improve errors
    {
        // SAFETY: The bounds must be larger than or equal
        unsafe { Refined::new_unchecked(self.to_int()) }
    }

    #[inline]
    /// # Safety
    /// `self` must be in S2..E2
    pub unsafe fn shrink<S2: Unsigned, E2: Unsigned>(self) -> Refined<T, S2, E2> {
        // SAFETY: Enforced by the safety invariants of `shrink`
        unsafe { Refined::new_unchecked(self.to_int()) }
    }
}

impl<T, S, E> Refined<T, S, E>
where
    T: PartialOrd,
    S: Unsigned + ToInt<T>,
    E: Unsigned + ToInt<T>,
{
    #[inline]
    pub fn new(num: T) -> Option<Self> {
        if num >= S::to_int() && num < E::to_int() {
            // SAFETY: We checked that `num` is within S..E
            unsafe { Some(Refined::new_unchecked(num)) }
        } else {
            None
        }
    }
}

impl<T, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub const fn constant<const N: usize>() -> Self
    where
        S: IsLessOrEqual<<Const<N> as ToUInt>::Output>,
        Const<N>: ToUInt,
        <Const<N> as ToUInt>::Output: Unsigned + ToInt<T> + IsLessOrEqual<E>,
        LeEq<S, <Const<N> as ToUInt>::Output>: NonZero,
        LeEq<<Const<N> as ToUInt>::Output, E>: NonZero,
    {
        // SAFETY: Checked in the type bounds
        unsafe { Refined::new_unchecked(<Const<N> as ToUInt>::Output::INT) }
    }
}

impl<T: UnsignedType, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub fn cast<T2: UnsignedType>(self) -> Refined<T2, S, E>
    where
        T2::SIZE: IsGreaterOrEqual<E>,
        GrEq<T2::SIZE, E>: NonZero,
        T: TryInto<T2>,
    {
        // SAFETY: We check it can't overflow with the `IsGreater` type bound
        let cast = unsafe { self.to_int().try_into().unwrap_unchecked() };
        // SAFETY: The bounds haven't changed
        unsafe { Refined::new_unchecked(cast) }
    }
}

impl<T: SimdElement, S: Unsigned, E: Unsigned, const LANES: usize> Refined<Simd<T, LANES>, S, E>
where
    LaneCount<LANES>: SupportedLaneCount,
{
    #[inline]
    pub fn cast<T2: SimdCast>(self) -> Refined<Simd<T2, LANES>, S, E>
    where
        Simd<T, LANES>: SimdUint<Cast<T2> = Simd<T2, LANES>>,
    {
        // SAFETY: The bounds haven't changed
        unsafe { Refined::new_unchecked(self.to_int().cast::<T2>()) }
    }
}

// TODO: Combine these via passing to another trait so you can be generic over Simd
impl<T: Copy + UnsignedType + PartialOrd, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub fn split<M: Unsigned + ToInt<T>, R>(
        self,
        left: impl Fn(Refined<T, S, M>) -> R,
        right: impl Fn(Refined<T, M, E>) -> R,
    ) -> R {
        if self.to_int() < M::to_int() {
            // SAFETY: We just checked if it is in this section of S..E
            left(unsafe { self.shrink() })
        } else {
            // SAFETY: We just checked if it is in this section of S..E
            right(unsafe { self.shrink() })
        }
    }
}

impl<T: SimdElement, S: Unsigned, E: Unsigned, const LANES: usize> Refined<Simd<T, LANES>, S, E>
where
    LaneCount<LANES>: SupportedLaneCount,
    Simd<T, LANES>: Copy + SimdPartialOrd + SimdPartialEq<Mask = Mask<T::Mask, LANES>>,
{
    #[inline]
    pub fn split<M: Unsigned + ToInt<T>, S2: Unsigned, E2: Unsigned>(
        self,
        left: impl Fn(Refined<Simd<T, LANES>, S, M>) -> Refined<Simd<T, LANES>, S2, E2>,
        right: impl Fn(Refined<Simd<T, LANES>, M, E>) -> Refined<Simd<T, LANES>, S2, E2>,
    ) -> Refined<Simd<T, LANES>, S2, E2> {
        let mask = self.to_int().simd_lt(Simd::splat(M::to_int()));
        let unrefined = mask.select(
            // SAFETY: We just checked if it is in this section of S..E
            left(unsafe { self.shrink() }).to_int(),
            // SAFETY: We just checked if it is in this section of S..E
            right(unsafe { self.shrink() }).to_int(),
        );
        // SAFETY: `left` and `right` return this refined so it must have been generated correctly
        unsafe { Refined::new_unchecked(unrefined) }
    }
}

impl<T: SimdElement, S: Unsigned, E: Unsigned> Refined<T, S, E> {
    #[inline]
    pub fn simd_splat<const LANES: usize>(self) -> Refined<Simd<T, LANES>, S, E>
    where
        LaneCount<LANES>: SupportedLaneCount,
    {
        // SAFETY: The bounds haven't changed
        unsafe { Refined::new_unchecked(Simd::splat(self.to_int())) }
    }
}

impl<T: Debug, S: Unsigned + ToInt<T>, E: Unsigned + ToInt<T>> Debug for Refined<T, S, E> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?} is {:?}..{:?}", self.0, S::to_int(), E::to_int())
    }
}

impl<T, S, E, N> Add<N> for Refined<T, S, E>
where
    T: UnsignedType + Add<Output = T>,
    S: Unsigned + Add<N::Output>,
    E: Unsigned + Add<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
{
    type Output = Refined<T, <S as Add<N::Output>>::Output, <E as Add<N::Output>>::Output>;
    fn add(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we added N to both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() + N::Output::to_int()) }
    }
}

impl<T, S, E, N> Sub<N> for Refined<T, S, E>
where
    T: UnsignedType + Sub<Output = T>,
    S: Unsigned + Sub<N::Output>,
    E: Unsigned + Sub<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
{
    type Output = Refined<T, <S as Sub<N::Output>>::Output, <E as Sub<N::Output>>::Output>;
    fn sub(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we subtracted N from both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() - N::Output::to_int()) }
    }
}

pub trait UnsignedType: sealed::Sealed + Copy {
    type SIZE: Unsigned;
    const ONE: Self;
}
impl UnsignedType for u8 {
    type SIZE = U256;
    const ONE: Self = 1;
}
impl UnsignedType for u16 {
    type SIZE = <U256 as Pow<U2>>::Output;
    const ONE: Self = 1;
}
impl UnsignedType for u32 {
    type SIZE = <U256 as Pow<U4>>::Output;
    const ONE: Self = 1;
}
impl UnsignedType for u64 {
    type SIZE = <U256 as Pow<U8>>::Output;
    const ONE: Self = 1;
}
impl UnsignedType for u128 {
    type SIZE = <U256 as Pow<U16>>::Output;
    const ONE: Self = 1;
}

mod sealed {
    pub trait Sealed {}
    impl Sealed for u8 {}
    impl Sealed for u16 {}
    impl Sealed for u32 {}
    impl Sealed for u64 {}
    impl Sealed for u128 {}
}

impl<T, S, E, N, const LANES: usize> Add<N> for Refined<Simd<T, LANES>, S, E>
where
    T: SimdElement,
    S: Unsigned + Add<N::Output>,
    E: Unsigned + Add<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
    Simd<T, LANES>: Add<Output = Simd<T, LANES>>,
    LaneCount<LANES>: SupportedLaneCount,
{
    type Output =
        Refined<Simd<T, LANES>, <S as Add<N::Output>>::Output, <E as Add<N::Output>>::Output>;
    fn add(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we added N to both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() + Simd::splat(N::Output::to_int())) }
    }
}

impl<T, S, E, N, const LANES: usize> Sub<N> for Refined<Simd<T, LANES>, S, E>
where
    T: SimdElement,
    S: Unsigned + Sub<N::Output>,
    E: Unsigned + Sub<N::Output>,
    N: ToUInt,
    S::Output: Unsigned,
    E::Output: Unsigned,
    N::Output: ToInt<T>,
    Simd<T, LANES>: Sub<Output = Simd<T, LANES>>,
    LaneCount<LANES>: SupportedLaneCount,
{
    type Output =
        Refined<Simd<T, LANES>, <S as Sub<N::Output>>::Output, <E as Sub<N::Output>>::Output>;
    fn sub(self, _: N) -> Self::Output {
        // SAFETY: `self` was within bounds (S..E) and we subtracted N from both these bounds so it is still within them
        unsafe { Refined::new_unchecked(self.to_int() - Simd::splat(N::Output::to_int())) }
    }
}

impl<T, S1, E1, S2, E2> Add<Refined<T, S2, E2>> for Refined<T, S1, E1>
where
    T: Add<Output = T>,
    S1: Unsigned + Add<S2>,
    E1: Unsigned + Add<E2>,
    S2: Unsigned,
    E2: Unsigned,
    S1::Output: Unsigned,
    E1::Output: Unsigned + Sub<B1>,
    <E1::Output as Sub<B1>>::Output: Unsigned,
{
    type Output = Refined<T, <S1 as Add<S2>>::Output, Sub1<<E1 as Add<E2>>::Output>>;
    fn add(self, rhs: Refined<T, S2, E2>) -> Self::Output {
        // SAFETY: We have widened the bounds so that S1 + S2 and (E1 - 1) + (E2 - 1) are within bounds so this must be
        unsafe { Refined::new_unchecked(self.to_int() + rhs.to_int()) }
    }
}

impl<T, S1, E1, S2, E2> Sub<Refined<T, S2, E2>> for Refined<T, S1, E1>
where
    T: Sub<Output = T>,
    S1: Unsigned + Sub<E2>,
    E1: Unsigned + Sub<S2>,
    S2: Unsigned,
    E2: Unsigned,
    S1::Output: Unsigned + Add<B1>,
    E1::Output: Unsigned,
    <S1::Output as Add<B1>>::Output: Unsigned,
{
    type Output = Refined<T, Add1<<S1 as Sub<E2>>::Output>, <E1 as Sub<S2>>::Output>;
    fn sub(self, rhs: Refined<T, S2, E2>) -> Self::Output {
        // SAFETY: We have widened the bounds so that it is within (S1 - (E2 - 1))..(E1 - S2)
        unsafe { Refined::new_unchecked(self.to_int() - rhs.to_int()) }
    }
}

impl<T, S1, E1, S2, E2> Mul<Refined<T, S2, E2>> for Refined<T, S1, E1>
where
    T: Mul<Output = T>,
    S1: Unsigned + Mul<S2>,
    S2: Unsigned,
    E1: Unsigned + Sub<B1>,
    E2: Unsigned + Sub<B1>,
    S1::Output: Unsigned,
    E1::Output: Unsigned + Mul<E2::Output>,
    E2::Output: Unsigned,
    <E1::Output as Mul<E2::Output>>::Output: Unsigned + Add<B1>,
    <<E1::Output as Mul<E2::Output>>::Output as Add<B1>>::Output: Unsigned,
{
    type Output = Refined<T, <S1 as Mul<S2>>::Output, Add1<<Sub1<E1> as Mul<Sub1<E2>>>::Output>>;
    fn mul(self, rhs: Refined<T, S2, E2>) -> Self::Output {
        // SAFETY: The bounds have been widened to (S1 * S2)..((E1 - 1) * (E2 - 1) + 1)
        unsafe { Refined::new_unchecked(self.to_int() * rhs.to_int()) }
    }
}

impl<T, S, E, N> Rem<N> for Refined<T, S, E>
where
    T: UnsignedType + Rem<Output = T>,
    S: Unsigned,
    E: Unsigned,
    N: ToUInt,
    N::Output: Unsigned + ToInt<T>,
{
    type Output = Refined<T, U0, N::Output>;
    fn rem(self, _: N) -> Self::Output {
        // SAFETY: Modular arithmetic cannot be larger than its modulus (N)
        unsafe { Refined::new_unchecked(self.to_int() % N::Output::to_int()) }
    }
}

impl<T, S, E, N, const LANES: usize> Rem<N> for Refined<Simd<T, LANES>, S, E>
where
    T: SimdElement,
    S: Unsigned,
    E: Unsigned,
    N: ToUInt,
    N::Output: Unsigned + ToInt<T>,
    Simd<T, LANES>: Rem<Output = Simd<T, LANES>>,
    LaneCount<LANES>: SupportedLaneCount,
{
    type Output = Refined<Simd<T, LANES>, U0, N::Output>;
    fn rem(self, _: N) -> Self::Output {
        // SAFETY: Modular arithmetic cannot be larger than its modulus (N)
        unsafe { Refined::new_unchecked(self.to_int() % Simd::splat(N::Output::to_int())) }
    }
}

impl<I, T, S, E, const N: usize> Index<Refined<I, S, E>> for [T; N]
where
    I: Into<usize>,
    S: Unsigned,
    E: Unsigned,
    Const<N>: ToUInt,
    <Const<N> as ToUInt>::Output: IsGreaterOrEqual<E>,
    GrEq<<Const<N> as ToUInt>::Output, E>: NonZero,
{
    type Output = T;
    fn index(&self, index: Refined<I, S, E>) -> &T {
        // SAFETY: The bounds are checked by the trait bounds (specifically GrEq)
        unsafe { self.get_unchecked(index.to_int().into()) }
    }
}

impl<I, T, S, E, const N: usize> IndexMut<Refined<I, S, E>> for [T; N]
where
    I: Into<usize>,
    S: Unsigned,
    E: Unsigned,
    Const<N>: ToUInt,
    <Const<N> as ToUInt>::Output: IsGreaterOrEqual<E>,
    GrEq<<Const<N> as ToUInt>::Output, E>: NonZero,
{
    fn index_mut(&mut self, index: Refined<I, S, E>) -> &mut T {
        // SAFETY: The bounds are checked by the trait bounds (specifically GrEq)
        unsafe { self.get_unchecked_mut(index.to_int().into()) }
    }
}

pub trait RefinedSimdExt<T: SimdElement, S: Unsigned, E: Unsigned> {
    fn truncate_as_simd<const LANES: usize>(&self) -> Refined<Simd<T, LANES>, S, E>
    where
        LaneCount<LANES>: SupportedLaneCount;

    #[allow(clippy::type_complexity)]
    fn as_ref_simd<const LANES: usize>(
        &self,
    ) -> (
        &[Refined<T, S, E>],
        &[Refined<Simd<T, LANES>, S, E>],
        &[Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsRef<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount;

    #[allow(clippy::type_complexity)]
    fn as_ref_simd_mut<const LANES: usize>(
        &mut self,
    ) -> (
        &mut [Refined<T, S, E>],
        &mut [Refined<Simd<T, LANES>, S, E>],
        &mut [Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsMut<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount;
}

impl<T: SimdElement, S: Unsigned, E: Unsigned> RefinedSimdExt<T, S, E> for [Refined<T, S, E>] {
    #[inline]
    fn truncate_as_simd<const LANES: usize>(&self) -> Refined<Simd<T, LANES>, S, E>
    where
        LaneCount<LANES>: SupportedLaneCount,
    {
        let unrefined = ptr::from_ref(self) as *const [T];
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let simd = unsafe { Simd::from_slice(&*unrefined) };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        unsafe { cursed_transmute(simd) }
    }

    #[allow(clippy::type_complexity)]
    #[inline]
    fn as_ref_simd<const LANES: usize>(
        &self,
    ) -> (
        &[Refined<T, S, E>],
        &[Refined<Simd<T, LANES>, S, E>],
        &[Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsRef<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount,
    {
        let unrefined = ptr::from_ref(self) as *const [T];
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let (prefix, middle, suffix) = unsafe { (*unrefined).as_simd() };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let prefix = unsafe { &*(ptr::from_ref(prefix) as *const [Refined<T, S, E>]) };
        // SAFETY: `Simd` doesn't change the values so they are still in S..E and `Refined` is repr(transparent)
        let middle = unsafe { &*(ptr::from_ref(middle) as *const [Refined<Simd<T, LANES>, S, E>]) };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let suffix = unsafe { &*(ptr::from_ref(suffix) as *const [Refined<T, S, E>]) };
        (prefix, middle, suffix)
    }

    #[allow(clippy::type_complexity)]
    #[inline]
    fn as_ref_simd_mut<const LANES: usize>(
        &mut self,
    ) -> (
        &mut [Refined<T, S, E>],
        &mut [Refined<Simd<T, LANES>, S, E>],
        &mut [Refined<T, S, E>],
    )
    where
        Simd<T, LANES>: AsMut<[T; LANES]>,
        LaneCount<LANES>: SupportedLaneCount,
    {
        let unrefined = ptr::from_mut(self) as *mut [T];
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let (prefix, middle, suffix) = unsafe { (*unrefined).as_simd_mut() };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let prefix = unsafe { &mut *(ptr::from_ref(prefix) as *mut [Refined<T, S, E>]) };
        // SAFETY: `Simd` doesn't change the values so they are still in S..E and `Refined` is repr(transparent)
        let middle =
            unsafe { &mut *(ptr::from_ref(middle) as *mut [Refined<Simd<T, LANES>, S, E>]) };
        // SAFETY: `Refined<T, _, _>` is a repr(transparent) wrapper over `T`
        let suffix = unsafe { &mut *(ptr::from_ref(suffix) as *mut [Refined<T, S, E>]) };
        (prefix, middle, suffix)
    }
}

/// Allows types that can't be size-checked pre-monomorphisation and allows you to downcast to a smaller size type.
/// # Safety
/// Same as `mem::transmute`
pub const unsafe fn cursed_transmute<Src, Dst>(src: Src) -> Dst {
    struct Check<Src, Dst>(Src, Dst);
    impl<Src, Dst> Check<Src, Dst> {
        const SIZE_MISMATCH: () = assert!(
            size_of::<Src>() <= size_of::<Dst>(),
            "Size mismatch in cursed_transmute"
        );
    }
    let _: () = Check::<Src, Dst>::SIZE_MISMATCH;

    #[allow(clippy::items_after_statements)]
    #[repr(C)]
    union Transmute<From, To> {
        from: ManuallyDrop<From>,
        to: ManuallyDrop<To>,
    }

    // SAFETY: `Check` ensures the sizes are valid, everything else is a safety invariant
    let manual_dst: ManuallyDrop<Dst> = unsafe {
        Transmute {
            from: ManuallyDrop::new(src),
        }
        .to
    };
    ManuallyDrop::into_inner(manual_dst)
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct ConstRange<T: UnsignedType, S: Unsigned, E: Unsigned>(Range<T>, PhantomData<(S, E)>);

impl<T, S, E> Default for ConstRange<T, S, E>
where
    T: UnsignedType,
    S: Unsigned + ToInt<T>,
    E: Unsigned + ToInt<T>,
{
    fn default() -> Self {
        ConstRange(S::to_int()..E::to_int(), PhantomData)
    }
}

impl<T, S, E> Iterator for ConstRange<T, S, E>
where
    T: UnsignedType + AddAssign + PartialOrd,
    S: Unsigned + ToInt<usize>,
    E: Unsigned + ToInt<T> + ToInt<usize>,
    Range<T>: Iterator<Item = T>,
{
    type Item = Refined<T, S, E>;
    fn next(&mut self) -> Option<Refined<T, S, E>> {
        self.0.next().map(|u| {
            // SAFETY: ConstRange is only constructed via Default which makes `self.0` always in S..E
            unsafe { Refined::new_unchecked(u) }
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl<T, S, E> ExactSizeIterator for ConstRange<T, S, E>
where
    T: UnsignedType + AddAssign + PartialOrd,
    S: Unsigned + ToInt<usize>,
    E: Unsigned + ToInt<T> + ToInt<usize>,
    Range<T>: Iterator<Item = T>,
{
}

impl<T, S, E> FusedIterator for ConstRange<T, S, E>
where
    T: UnsignedType + AddAssign + PartialOrd,
    S: Unsigned + ToInt<usize>,
    E: Unsigned + ToInt<T> + ToInt<usize>,
    Range<T>: Iterator<Item = T>,
{
}
