use crate::modular::Modular;
use crate::refinement::Refined;
use typenum::{Const, U65, U91};

/// Calculated from `../texts/alice_in_wonderland.txt` and divided by 10
pub const LETTER_COUNTS: [u32; 26] = [
    1750, 293, 475, 983, 2704, 399, 505, 1470, 1495, 29, 231, 938, 420, 1399, 1625, 301, 41, 1080,
    1297, 2129, 691, 167, 534, 28, 452, 15,
];

#[must_use]
pub fn monogram_score(text: &[Refined<u8, U65, U91>]) -> u32 {
    let mut score = 0;
    for letter in text {
        score += LETTER_COUNTS[*letter - Const::<65>];
    }
    score
}

#[must_use]
pub fn ioc(text: &[Refined<u8, U65, U91>]) -> f32 {
    let mut counts = [0_u32; 26];
    for letter in text {
        counts[*letter - Const::<65>] += 1;
    }
    let score: f32 = counts.iter().map(|&c| c as f32 * (c as f32 - 1.0)).sum();
    let length = text.len() as f32;
    score / (length * (length - 1.0))
}

#[must_use]
pub fn periodic_ioc(text: &[Refined<u8, U65, U91>], period: usize) -> f32 {
    let mut counts = vec![0_u32; 26 * period];
    let mut index = 0;
    // Due to the runtime period we can't really refine this
    for letter in text {
        // SAFETY: letter - 65 will always be in 0..26 (ascii capital) so we can always index counts with it
        unsafe { *counts.get_unchecked_mut(index + letter.to_int() as usize - 65) += 1 };
        index = index.mod_inc(26, 26 * period);
    }

    let score: f32 = counts.iter().map(|&c| c as f32 * (c as f32 - 1.0)).sum();
    let length = text.len() as f32 / period as f32;
    score / (length * (length - 1.0) * period as f32)
}
