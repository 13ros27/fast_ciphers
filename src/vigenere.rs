use crate::{
    analyse::monogram_score,
    caesar::{decrypt_char, encrypt_char, Caesar},
    modular::Modular,
    refinement::{Refined, RefinedSimdExt},
    traits::{Cipher, CrackKl, KeySpace},
    utils::Transpose,
};
use std::simd::{LaneCount, SupportedLaneCount};
use typenum::{Const, U0, U26, U65, U91};

#[derive(Default)]
pub struct Vigenere;

impl Cipher for Vigenere {
    type Key<'a> = &'a [Refined<u8, U0, U26>];
    type OwnedKey = Box<[Refined<u8, U0, U26>]>;

    fn decrypt<const N: usize>(
        &self,
        text: &mut [Refined<u8, U65, U91>],
        key: &[Refined<u8, U0, U26>],
    ) where
        LaneCount<N>: SupportedLaneCount,
    {
        let (prefix, middle, suffix) = text.as_ref_simd_mut();
        let key_length = key.len();
        let mut key_index = 0;
        for letter in prefix.iter_mut() {
            *letter = decrypt_char(*letter, key[key_index]);
            key_index = key_index.mod_inc(1, key_length);
        }

        let extended_key = &*key
            .iter()
            .copied()
            .cycle()
            .take(N + key_length)
            .collect::<Vec<_>>();
        let increment = N % key_length;
        for text in middle.iter_mut() {
            let simd_key = extended_key[key_index..].truncate_as_simd();
            key_index = key_index.mod_inc(increment, key_length);

            let new_text = *text - simd_key;
            *text = new_text.split::<U65, _, _>(|t| (t + Const::<26>).widen(), |t| t);
            // *text -= simd_key;
            // let mask = text.simd_lt(Simd::splat(b'A'));
            // *text = mask.select(*text + Simd::splat(26), *text);
        }

        for letter in suffix.iter_mut() {
            *letter = decrypt_char(*letter, key[key_index]);
            key_index = key_index.mod_inc(1, key_length);
        }
    }

    fn encrypt<const N: usize>(
        &self,
        text: &mut [Refined<u8, U65, U91>],
        key: &[Refined<u8, U0, U26>],
    ) where
        LaneCount<N>: SupportedLaneCount,
    {
        let (prefix, middle, suffix) = text.as_ref_simd_mut();
        let key_length = key.len();
        let mut key_index = 0;
        for letter in prefix.iter_mut() {
            *letter = encrypt_char(*letter, key[key_index]);
            key_index = key_index.mod_inc(1, key_length);
        }

        let extended_key = &*key
            .iter()
            .copied()
            .cycle()
            .take(N + key_length)
            .collect::<Vec<_>>();
        let increment = N % key_length;
        for text in middle.iter_mut() {
            let simd_key = extended_key[key_index..].truncate_as_simd();
            key_index = key_index.mod_inc(increment, key_length);

            let new_text = *text + simd_key;
            *text = new_text.split::<U91, _, _>(|t| t, |t| (t - Const::<26>).widen());
            // *text += simd_key;
            // let mask = text.simd_gt(Simd::splat(b'Z'));
            // *text = mask.select(*text - Simd::splat(26), *text);
        }

        for letter in suffix.iter_mut() {
            *letter = encrypt_char(*letter, key[key_index]);
            key_index = key_index.mod_inc(1, key_length);
        }
    }
}

impl CrackKl for Vigenere {
    fn find_key_kl<const N: usize>(
        &self,
        text: &[Refined<u8, U65, U91>],
        kl: u8,
    ) -> Box<[Refined<u8, U0, U26>]>
    where
        LaneCount<N>: SupportedLaneCount,
    {
        let mut best_key: Box<[_]> = vec![Refined::constant::<0>(); kl as usize].into();
        let mut best_score: Box<[_]> = vec![0; kl as usize].into();
        let mut short_text = text[..64 * kl as usize].to_owned();
        for key in Caesar.keyspace() {
            for (i, score) in short_text
                .transpose(kl as u32)
                .iter()
                .map(|s| monogram_score(&s.copied().collect::<Vec<_>>()))
                .enumerate()
            {
                if score > best_score[i] {
                    best_score[i] = score;
                    best_key[i] = key;
                }
            }
            Caesar.decrypt(&mut short_text, Refined::constant::<1>());
        }
        best_key
    }
}
