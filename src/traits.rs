use crate::refinement::Refined;
use std::simd::{LaneCount, SupportedLaneCount};
use typenum::{U65, U91};

pub trait Cipher {
    type Key<'a>: Copy;
    type OwnedKey: Clone + for<'a> CanBorrow<'a, Self::Key<'a>>;

    fn decrypt<const N: usize>(&self, text: &mut [Refined<u8, U65, U91>], key: Self::Key<'_>)
    where
        LaneCount<N>: SupportedLaneCount;
    fn encrypt<const N: usize>(&self, text: &mut [Refined<u8, U65, U91>], key: Self::Key<'_>)
    where
        LaneCount<N>: SupportedLaneCount;
}

pub trait Crack: Cipher {
    /// Find the key without doing the often expensive decryption
    fn find_key<const N: usize>(&self, text: &[Refined<u8, U65, U91>]) -> Self::OwnedKey
    where
        LaneCount<N>: SupportedLaneCount;

    fn crack<const N: usize>(&self, text: &mut [Refined<u8, U65, U91>]) -> Self::OwnedKey
    where
        LaneCount<N>: SupportedLaneCount,
    {
        let key = self.find_key(text);
        self.decrypt(text, key.borrow());
        key
    }
}

pub trait CrackKl: Cipher {
    fn find_key_kl<const N: usize>(&self, text: &[Refined<u8, U65, U91>], kl: u8) -> Self::OwnedKey
    where
        LaneCount<N>: SupportedLaneCount;

    fn crack_kl<const N: usize>(&self, text: &mut [Refined<u8, U65, U91>], kl: u8) -> Self::OwnedKey
    where
        LaneCount<N>: SupportedLaneCount,
    {
        let key = self.find_key_kl(text, kl);
        self.decrypt(text, key.borrow());
        key
    }
}

pub trait KeySpace: Cipher {
    fn keyspace(&self) -> impl Iterator<Item = Self::OwnedKey>;
}

pub trait CanBorrow<'a, T: ?Sized + 'a> {
    fn borrow(&'a self) -> T;
}
impl<'a, T: Copy + ?Sized + 'a> CanBorrow<'a, T> for T {
    fn borrow(&self) -> Self {
        *self
    }
}
impl<'a, T> CanBorrow<'a, &'a [T]> for Box<[T]> {
    fn borrow(&'a self) -> &'a [T] {
        self
    }
}
