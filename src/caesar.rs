use crate::analyse::monogram_score;
use crate::refinement::{ConstRange, Refined, RefinedSimdExt};
use crate::traits::{Cipher, Crack, KeySpace};
use std::simd::{LaneCount, SupportedLaneCount};
use typenum::{Const, U0, U26, U65, U91};

#[derive(Default)]
pub struct Caesar;

pub(crate) fn decrypt_char(
    text: Refined<u8, U65, U91>,
    key: Refined<u8, U0, U26>,
) -> Refined<u8, U65, U91> {
    let text = text - key;
    text.split::<U65, _>(|t| (t + Const::<26>).widen(), |t| t)
}

pub(crate) fn encrypt_char(
    text: Refined<u8, U65, U91>,
    key: Refined<u8, U0, U26>,
) -> Refined<u8, U65, U91> {
    let text = text + key;
    text.split::<U91, _>(|t| t, |t| (t - Const::<26>).widen())
    // if text > b'Z' {
    //     text -= 26;
    // }
    // text
}

impl KeySpace for Caesar {
    fn keyspace(&self) -> impl Iterator<Item = Refined<u8, U0, U26>> {
        ConstRange::<u8, U0, U26>::default()
    }
}

impl Cipher for Caesar {
    type Key<'a> = Refined<u8, U0, U26>;
    type OwnedKey = Refined<u8, U0, U26>;

    fn decrypt<const N: usize>(&self, text: &mut [Refined<u8, U65, U91>], key: Refined<u8, U0, U26>)
    // fn decrypt<const N: usize>(&self, text: &mut [u8], key: u8)
    where
        LaneCount<N>: SupportedLaneCount,
    {
        // text.iter_mut().for_each(|l| *l = decrypt_char(*l, key));
        let (prefix, middle, suffix) = text.as_ref_simd_mut();
        prefix.iter_mut().for_each(|l| *l = decrypt_char(*l, key));
        for text in middle.iter_mut() {
            // println!("Middle path");
            let new_text = *text - key.simd_splat();
            *text = new_text.split::<U65, _, _>(|t| (t + Const::<26>).widen(), |t| t);
            // *text -= Simd::splat(key);
            // let mask = text.simd_lt(Simd::splat(b'A'));
            // *text = mask.select(*text + Simd::splat(26), *text);
        }
        suffix.iter_mut().for_each(|l| *l = decrypt_char(*l, key));
    }

    fn encrypt<const N: usize>(&self, text: &mut [Refined<u8, U65, U91>], key: Refined<u8, U0, U26>)
    where
        LaneCount<N>: SupportedLaneCount,
    {
        let (prefix, middle, suffix) = text.as_ref_simd_mut();
        prefix.iter_mut().for_each(|l| *l = encrypt_char(*l, key));
        for text in middle.iter_mut() {
            let new_text = *text + key.simd_splat();
            *text = new_text.split::<U91, _, _>(|t| t, |t| (t - Const::<26>).widen());
            // *text += Simd::splat(key);
            // let mask = text.simd_gt(Simd::splat(b'Z'));
            // *text = mask.select(*text - Simd::splat(26), *text);
        }
        suffix.iter_mut().for_each(|l| *l = encrypt_char(*l, key));
    }
}

impl Crack for Caesar {
    fn find_key<const N: usize>(&self, text: &[Refined<u8, U65, U91>]) -> Refined<u8, U0, U26>
    where
        LaneCount<N>: SupportedLaneCount,
    {
        let mut best_key = Refined::constant::<0>();
        let mut best_score = 0;
        let mut short_text = text.copy_aligned_64();
        for key in self.keyspace() {
            let score = monogram_score(&short_text);
            if score > best_score {
                best_score = score;
                best_key = key;
            }
            self.decrypt(&mut short_text, Refined::constant::<1>());
        }
        best_key
    }
}

trait CopyAlignedExt<T> {
    fn copy_aligned_64(&self) -> Box<Aligned<[T]>>;
}

impl<T: Copy + std::fmt::Debug> CopyAlignedExt<T> for [T] {
    /// Copies the first 64 values from the passed slice, aligning them to 64 bytes (panic with less than 64 bytes)
    fn copy_aligned_64(&self) -> Box<Aligned<[T]>> {
        // let layout =
        //     std::alloc::Layout::from_size_align(self.len().min(64) * std::mem::size_of::<T>(), 64)
        //         .unwrap();
        // let raw_ptr = unsafe { std::alloc::alloc(layout) } as *mut T;
        // let raw_slice =
        //     unsafe { std::slice::from_raw_parts_mut(raw_ptr, self.len().min(64)) } as *mut [T];
        // unsafe { raw_slice.as_mut().unwrap_unchecked() }
        //     .copy_from_slice(&self[..self.len().min(64)]);
        // // println!("{:?}", unsafe { raw_slice.as_ref() });
        // // panic!();
        // // unsafe { Box::from_raw(raw_slice as *mut Aligned<[T]>) }
        // raw_slice as *mut Aligned<[T]>

        // SAFETY: Its length is 64 so we can definitely turn it into a [T; 64]
        let aligned: [T; 64] = unsafe { self[..64].try_into().unwrap_unchecked() };
        let leaked: *mut T = std::ptr::from_mut(Box::leak(Box::new(Aligned(aligned)))).cast();
        // SAFETY: It was created as a 64 element array in `aligned` so we can interpret it as a 64 element slice
        let slice = unsafe { std::slice::from_raw_parts_mut(leaked, 64) };
        // SAFETY: The pointer was created in `leaked` so is valid and non-null
        let boxed = unsafe { Box::from_raw(std::ptr::from_mut(slice)) };
        // SAFETY: This pointer was created from an `Aligned` so it is valid to transmute it back into `Aligned`
        // (so that Box::drop gets the correct alignment)
        unsafe { std::mem::transmute(boxed) }
    }
}

#[derive(Copy, Clone, Debug)]
#[repr(C, align(64))]
struct Aligned<T: ?Sized>(T);

impl<T: ?Sized> std::ops::Deref for Aligned<T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T: ?Sized> std::ops::DerefMut for Aligned<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.0
    }
}
