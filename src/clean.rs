use crate::refinement::Refined;
use typenum::{Const, U123, U65, U91, U97};

#[must_use]
pub fn clean_upper(text: &str) -> Box<[Refined<u8, U65, U91>]> {
    text.bytes()
        .filter_map(|l| {
            #[allow(clippy::manual_map)] // Feels bad here because of the double if
            if let Some(letter) = Refined::<_, U65, U91>::new(l) {
                Some(letter)
            } else if let Some(letter) = Refined::<_, U97, U123>::new(l) {
                Some(letter - Const::<32>)
            } else {
                None
            }
        })
        .collect()
}
