use std::borrow::Borrow;

pub trait Transpose<T> {
    fn transpose(&self, sections: u32) -> Box<[Section<'_, T>]>;
}

impl<T, B: Borrow<[T]>> Transpose<T> for B {
    fn transpose(&self, sections: u32) -> Box<[Section<'_, T>]> {
        (0..sections)
            .map(|i| Section {
                text: self.borrow(),
                sections,
                index: i,
            })
            .collect()
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Section<'a, T> {
    text: &'a [T],
    sections: u32,
    index: u32,
}

impl<'a, T> Iterator for Section<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<&'a T> {
        let ret = self.text.get(self.index as usize);
        self.index += self.sections;
        ret
    }
}
