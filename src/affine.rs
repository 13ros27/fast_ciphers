use crate::analyse::monogram_score;
use crate::caesar::Caesar;
use crate::refinement::{ConstRange, Refined, RefinedSimdExt};
use crate::traits::{Cipher, Crack, KeySpace};
use std::simd::{LaneCount, SupportedLaneCount};
use typenum::{Const, U0, U26, U65, U91};

const MMI_26: [Refined<u16, U0, U26>; 26] = [
    Refined::constant::<0>(),
    Refined::constant::<1>(),
    Refined::constant::<0>(),
    Refined::constant::<9>(),
    Refined::constant::<0>(),
    Refined::constant::<21>(),
    Refined::constant::<0>(),
    Refined::constant::<15>(),
    Refined::constant::<0>(),
    Refined::constant::<3>(),
    Refined::constant::<0>(),
    Refined::constant::<19>(),
    Refined::constant::<0>(),
    Refined::constant::<0>(),
    Refined::constant::<0>(),
    Refined::constant::<7>(),
    Refined::constant::<0>(),
    Refined::constant::<23>(),
    Refined::constant::<0>(),
    Refined::constant::<11>(),
    Refined::constant::<0>(),
    Refined::constant::<5>(),
    Refined::constant::<0>(),
    Refined::constant::<17>(),
    Refined::constant::<0>(),
    Refined::constant::<25>(),
];
const VALID_KEYS: [Refined<u8, U0, U26>; 12] = [
    Refined::constant::<1>(),
    Refined::constant::<3>(),
    Refined::constant::<5>(),
    Refined::constant::<7>(),
    Refined::constant::<9>(),
    Refined::constant::<11>(),
    Refined::constant::<15>(),
    Refined::constant::<17>(),
    Refined::constant::<19>(),
    Refined::constant::<21>(),
    Refined::constant::<23>(),
    Refined::constant::<25>(),
];

#[derive(Default)]
pub struct Affine;

impl KeySpace for Affine {
    fn keyspace(&self) -> impl Iterator<Item = (Refined<u8, U0, U26>, Refined<u8, U0, U26>)> {
        VALID_KEYS
            .iter()
            .flat_map(|&a| ConstRange::<u8, U0, U26>::default().map(move |b| (a, b)))
    }
}

impl Cipher for Affine {
    type Key<'a> = (Refined<u8, U0, U26>, Refined<u8, U0, U26>);
    type OwnedKey = (Refined<u8, U0, U26>, Refined<u8, U0, U26>);

    fn decrypt<const N: usize>(
        &self,
        text: &mut [Refined<u8, U65, U91>],
        (a, b): (Refined<u8, U0, U26>, Refined<u8, U0, U26>),
    ) where
        LaneCount<N>: SupportedLaneCount,
    {
        let (prefix, middle, suffix) = text.as_ref_simd_mut();
        let inv = MMI_26[a];
        for letter in prefix.iter_mut() {
            *letter = ((inv * (*letter - Const::<39> - b).cast::<u16>()) % Const::<26>)
                .cast::<u8>()
                + Const::<65>;
        }

        let simd_inv = inv.simd_splat();
        let simd_b = b.cast::<u16>().simd_splat();
        for text in middle.iter_mut() {
            let new_text = (*text - Const::<39>).cast(); // Use [39] not [65] so we stay positive
            let new_text = simd_inv * (new_text - simd_b);
            *text = (new_text % Const::<26>).cast() + Const::<65>;
        }

        for letter in suffix.iter_mut() {
            *letter = ((inv * (*letter - Const::<39> - b).cast::<u16>()) % Const::<26>)
                .cast::<u8>()
                + Const::<65>;
        }
    }

    fn encrypt<const N: usize>(
        &self,
        text: &mut [Refined<u8, U65, U91>],
        (a, b): (Refined<u8, U0, U26>, Refined<u8, U0, U26>),
    ) where
        LaneCount<N>: SupportedLaneCount,
    {
        let (prefix, middle, suffix) = text.as_ref_simd_mut();
        for letter in prefix.iter_mut() {
            *letter = ((a.cast::<u16>() * (*letter - Const::<65>).cast::<u16>() + b.cast::<u16>())
                % Const::<26>)
                .cast::<u8>()
                + Const::<65>;
        }

        for text in middle.iter_mut() {
            let new_text = (*text - Const::<65>).cast();
            let new_text = a.cast::<u16>().simd_splat() * new_text + b.cast::<u16>().simd_splat();
            *text = (new_text % Const::<26>).cast() + Const::<65>;
        }

        for letter in suffix.iter_mut() {
            *letter = ((a.cast::<u16>() * (*letter - Const::<65>).cast::<u16>() + b.cast::<u16>())
                % Const::<26>)
                .cast::<u8>()
                + Const::<65>;
        }
    }
}

impl Crack for Affine {
    fn find_key<const N: usize>(
        &self,
        text: &[Refined<u8, U65, U91>],
    ) -> (Refined<u8, U0, U26>, Refined<u8, U0, U26>)
    where
        LaneCount<N>: SupportedLaneCount,
    {
        let mut best_key = (Refined::constant::<0>(), Refined::constant::<0>());
        let mut best_score = 0;
        for inv in VALID_KEYS {
            let mut short_text = text[..64].to_owned();
            for letter in &mut short_text {
                *letter = ((inv.cast::<u16>() * (*letter - Const::<65>).cast()) % Const::<26>)
                    .cast::<u8>()
                    + Const::<65>;
            }
            for b in ConstRange::<u8, U0, U26>::default() {
                let score = monogram_score(&short_text);
                if score > best_score {
                    best_score = score;
                    best_key = (inv, b);
                }
                // Subtract inv not 1 because (inv * (l - 65 - b)) % 26 = (inv * (l - 65) - inv) % 26
                Caesar.decrypt(&mut short_text, inv);
            }
        }
        (MMI_26[best_key.0].cast(), best_key.1)
    }
}
