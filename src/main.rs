#![recursion_limit = "256"] // Thanks u128::SIZE
#![feature(associated_type_defaults, portable_simd)]
#![forbid(
    clippy::undocumented_unsafe_blocks,
    clippy::multiple_unsafe_ops_per_block
)]
#![deny(clippy::pedantic)]
#![allow(
    dead_code,
    clippy::cast_lossless,
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::module_name_repetitions
)]

mod affine;
mod analyse;
mod caesar;
mod clean;
mod modular;
mod refinement;
mod traits;
mod utils;
mod vigenere;

fn main() {
    println!("Hello, world!");
}
